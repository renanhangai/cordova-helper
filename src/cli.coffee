
# Helper class
Helper = require './helper'

# 
module.exports =
class CLI

	#
	# Console CLI
	# 
	@run: () ->
		args = process.argv.slice(2)
		if args.length < 0
			@printHelp()
			return

		command = args[0]
		basedir = process.cwd()
		if command == 'init'
			Helper.init basedir, () ->
			return
		else if command == 'create'
			Helper.create basedir, () ->
			return
		else if command == 'update'
			Helper.update basedir, () ->
			return
		else
			@printHelp()
			return
		
		
	@printHelp: () ->
		console.log "AMSKDMASKD"
