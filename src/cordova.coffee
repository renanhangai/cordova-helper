path = require 'path'
async = require 'async'
extend = require 'extend'
{exec}  = require 'child_process'

#
# Project class
#
module.exports =
class Project

	#
	# Construct the project
	# 
	constructor: ( data ) ->
		data ?= {}

		# Set properties
		@data_ = {}
		@data_.name      = data.name ? ""
		@data_.plugins   = data.plugins ? {}

	# Get the data
	getData: () -> @data_

	#
	# Ask for initial setup of the project
	# 
	askSetup: ( basedir, cb ) ->
		@readDefaults basedir, ( err, data ) =>
			# error
			if err
				cb err
				return
			
			# Name
			process.stdout.write "Enter project name(#{data.name}): "
			name = process.stdin.read()
			data.name = name if name

			# Save data
			@data_ = data

			cb null


	#
	# Read project default properties
	# 
	readDefaults: ( basedir, cb ) ->
		data = {}

		data.name = @data_.name
		if !data.name && basedir
			data.name = path.basename basedir

		deferrals = []

		# Plugins
		data.plugins = extend {}, @data_.plugins
		if basedir
			# Load plugins
			deferrals.push ( deferred_cb ) ->
				exec "cordova plugin ls", { cwd: basedir, env: process.env }, ( err, stdout, stderr ) ->
					if !err
						lines = stdout.split "\n"
						for line in lines
							continue if !line || line.match /^no plugins/i
							line = line.split /\s+/
							plugin  = line[0].trim()
							version = line[1].trim()
							if plugin
								data.plugins[plugin] = version
					deferred_cb()


		# Run all deferrals
		async.parallel deferrals, () ->
			cb( null, data )
			
