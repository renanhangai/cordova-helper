fs = require 'fs'
async = require 'async'
path = require 'path'
Project = require './cordova'
{spawn} = require 'child_process'

# Errors
ERROR =
	INVALID_JSON: 1
	CREATE_ALREADY_EXISTS: 2

#
# Helper class
# 
module.exports =
class Helper

	#
	# Initialize the project
	# 
	@init: ( basedir, cb ) ->
		jsonfile = path.join basedir, 'cordova.json'

		# Read the json
		@readJSON jsonfile, ( err, json ) =>
			# Error when reading file
			if err
				# Unhandled error
				if err.code != 'ENOENT'
					cb err
					return
				@create basedir, cb
				return

			# Deferrals
			deferrals = []
			
			# Plugins
			for plugin, version of json.plugins
				deferrals.push ( deferral_cb ) ->
					console.log "Adding plugin \"#{plugin}\""
					proc = spawn "cordova", ["plugin", "add", plugin], { cwd: basedir, env: process.env, stdio: 'inherit' }
					proc.on 'exit', ( err ) ->
						deferral_cb null
						 
			# Run in series
			async.series deferrals, ( err ) ->
				cb err
				

	#
	# Create the cordova project
	#
	@create: ( basedir, cb ) ->
		# Default arguments
		cb ?= ( err ) ->
			console.log err if err
			
		jsonfile = path.join basedir, 'cordova.json'
			
		# Read the json
		@readJSON jsonfile, ( err, json ) =>
			# Error when reading file
			if !err
				cb ERROR.CREATE_ALREADY_EXISTS
				return
			else if err.code != 'ENOENT'
				cb err
				return

			# Create the sample cordova
			cordova = new Project
			cordova.askSetup basedir, ( err ) ->
				fs.writeFileSync jsonfile, JSON.stringify(cordova.getData(), null, "\t"), "utf8"
				cb null

	#
	# Update
	# 
	@update: ( basedir, cb ) ->
		# Default arguments
		cb ?= ( err ) ->
			console.log err if err
			
		jsonfile = path.join basedir, 'cordova.json'
			
		# Read the json
		@readJSON jsonfile, ( err, json ) =>
			# Error when reading file
			if err
				cb err
				return

			# Update defaults for cordova
			cordova = new Project json
			cordova.readDefaults basedir, ( err, data ) ->
				fs.writeFileSync jsonfile, JSON.stringify(data, null, "\t"), "utf8"
				cb null

		
		
	#
	# Read the JSON
	# 
	@readJSON: ( jsonfile, cb ) ->
		# Read the json file for the project
		fs.readFile jsonfile, "utf8", ( err, data ) =>
			# Error when reading file
			if err
				cb err
				return

			# Read the project
			try 
				json = JSON.parse data
			catch e
				cb ERROR.INVALID_JSON
				return

			cb null, json

	#
	# Sample cordova project
	# 
	@sample: () ->
		cordova =
			name: ""
			plugins: {}
		return cordova
